import java.util.Scanner;
public class Problem7{
    static int factorial(int n){
        if(n==0)
        return 1;
    else
        return(n * factorial(n-1));
    }

public static void main(String args[]){
     Scanner input = new Scanner(System.in);

        System.out.println("Enter the desired number: ");

        String num = input.nextLine();

        int number = Integer.parseInt(num);
        int factor = factorial (number);

    System.out.println("Factorial of " + num + " is " + factor);
}
}