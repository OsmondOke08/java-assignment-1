import java.util.Scanner;
public class Problem3 {
    public static void main(String[] Strings){

        int convertMin = 60 * 24 * 365;

        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number of minutes: ");

        int mins = input.nextInt();

        int years = (mins / convertMin);
        int days = (mins / 60 / 24) % 365;

        System.out.println(mins + " minutes is equal to " + years + " years, and " + days + "days");


    }
}